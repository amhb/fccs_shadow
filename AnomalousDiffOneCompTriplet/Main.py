import os
import matplotlib.pyplot as plt
from SingleCurveFromFCS import AnomalDiffOneCompTriplet, kappa, Veff
from math import pi

path  = os.getcwd()

w0 = (Veff/kappa/pi**(3./2))**(1./3)
z0 = kappa*w0

x1, x2, y1, y2 = 0.00001, 1, -0.0005, 0.002


def readFiles(fileName):
    pathToFIle = os.path.join(path,fileName)
    k = open(pathToFIle)
    lines = k.readlines()
    k.close()
    return lines

def letsCreateModel(fileName):
    linesFormFile = readFiles(fileName)
    model = AnomalDiffOneCompTriplet(fileName,linesFormFile)
    return model

def printCurveWithFit (model):
    fig,ax=plt.subplots()
    plt.tick_params(labelsize=14)
    plt.axis((x1, x2, y1, y2))
    plt.title(model.fileName, fontsize=14)
    plt.plot(model.X, model.G,    "k.", label = "raw data")
    plt.plot(model.X, model.fitY, "r", label = "AnomalDiffOneCompTriplet")
    #D1 = w0**2/4/model.T1
    N = model.N
    a = model.alpha
    T1, T, Tt = model.T1*1000.0, model.T, model.Tt/1000000.0
    #txtstr=u' <N> = %d \n D1 = %.2f $\mu $$m^2/sec$ \n tauT = %.3f $\mu$ sec \n T = %.3f \n alpha = %.2f' %(N, D1, Tt, T, a)
    txtstr=u' <N> = %d \n T1 = %.3f msec \n Tt = %.3f $\mu$sec \n T = %.3f \n alpha = %.2f' %(N, T1, Tt, T, a)
    props= dict(boxstyle='round', facecolor="wheat", alpha=0.5)
    plt.text(0.65, 0.65, txtstr, transform=ax.transAxes, verticalalignment="top", bbox=props)
    plt.xscale('log', nonposy='clip')
    plt.xlabel(r"$\tau$ [sec]", fontsize = 18)
    plt.ylabel(r"G($\tau$)", fontsize=18)
    plt.legend(fancybox=True, shadow=True, loc=1)
    plt.grid(True)
    plt.tight_layout()
    fig = plt.gcf()
    fig.set_size_inches(7.5, 6)
    plt.savefig(os.path.join(path, "AnomalDiffOneCompTriplet_%s.png" %(model.fileName)))
    plt.clf()

def main():
    files = [f for f in os.listdir(path) if (os.path.isfile(f) & f.endswith(".dat"))]
    print "Total number of files", len(files)
    for f in files:
         Model1 = letsCreateModel(f)
         if Model1 is None:
             break
         else:
             printCurveWithFit(Model1)
    pass


if __name__ == '__main__':
    main()
