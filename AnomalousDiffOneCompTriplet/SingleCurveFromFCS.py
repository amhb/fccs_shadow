import os
from scipy.optimize import curve_fit
import numpy as np

#kappa = 3.474
#Veff = 0.398

kappa = 5.936
Veff = 0.372

path = os.getcwd()

class AnomalDiffOneCompTriplet(object):


    def __init__(self,name,filelines, initT=0.05, initTt=0.05, initT1=0.5, initN=0.5, initalpha=0.5):
        self.fileName = name
        self.X = self.parsowanieListForCurves("tau [sec]", filelines)
        self.G = self.parsowanieListForCurves("G(tau) CROSS", filelines)
        self.sd_G = self.parsowanieListForCurves("sd G(tau) CROSS", filelines)
        self.initials=[float(initT), float(initTt), float(initT1), float(initN), float(initalpha)]     #parametry poczatkowe
        self.wykonaj()
        
    def parsowanieListForCurves(self, key, filelines):
        firstLine =[k.replace("\"", "").replace("\n", "")  for k in filelines[0].split("\t")]
        column=firstLine.index(key)
        tempList = []
        for line in filelines[1:]:
            values = line.split()
            try:
                tempList.append(float(values[column].replace(",",".")))
            except IndexError:
                tempList.append(np.nan)
        return tempList

    def model_dyfuzji(self, x, T, Tt, T1, N, alpha): #AnomalDiffOneCompTriplet
        y=(1-T+T*np.exp(-x/Tt))*(1.0/(N*(1-T)))*((1+(x/T1)**alpha)**(-1))*( 1+ (x/T1)**alpha*(1/kappa**2))**(-0.5) #postac funkcji fitowanej
        return y

    def fitowanie(self):
        param_bounds=([0,0,0,0,0],[1,np.inf,np.inf,np.inf,1])
        try:
            fit= curve_fit(self.model_dyfuzji,self.X,self.G,self.initials, bounds=param_bounds)             #tu fituje
        except ValueError:
            print "fitowanie nie powiodlo sie. sprawdz czy X i Y sa rowej dlugosci"
            return 1
        try:
            perr=np.sqrt(np.diag(fit[1]))   #tu wyznacza bledy standardowe z macierzy kowarjancji (nieskonczona gdy fitowanie padnie 
        except ValueError:
            print "fitowanie nie powiodlo sie"
            return 1
        self.T=fit[0][0]
        self.Tt=fit[0][1]
        self.T1=fit[0][2]
        self.N=fit[0][3]
        self.alpha=fit[0][4]
        self.errT=perr[0]
        self.errTt=perr[1]
        self.errT1=perr[2]
        self.errN=perr[3]
        self.erralpha=perr[4]
        self.fitY=[self.model_dyfuzji(x,self.T, self.Tt, self.T1, self.N, self.alpha) for x in self.X] #wyznacza igreki dopasowania
        return 0

    def referuj(self):
        return[[self.T,self.Tt,self.T1, self.N, self.alpha],[self.errT, self.errTt, self.errT1, self.errN, self.erralpha],self.fitY]

    def wykonaj(self):
        self.fitowanie()
        return self.referuj() #zwraca liste list [[T,Tt,T1,N],[errT,errTt,errT1,errN],[igreki fitu]]



    
